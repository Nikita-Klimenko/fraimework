<?php

use \Framework\Model;

class User extends Model
{

    private $tableName = 'users';

    public function getAll(){
        $query = $this->connect->prepare("SELECT * FROM $this->tableName WHERE delete_user = '0'");
        $query->execute();
        $users = $query->fetchAll(PDO::FETCH_ASSOC);

       return $users;
    }

    public function getById($id){
        $query = $this->connect->prepare("SELECT * FROM $this->tableName WHERE id=?");
        $query->execute([$id]);
        $user = $query->fetch(PDO::FETCH_ASSOC);

        return $user;
    }

    public function getByEmail($email){
        $query = $this->connect->prepare("SELECT * FROM $this->tableName WHERE email=?");
        $query->execute([$email]);
        $user = $query->fetch(PDO::FETCH_ASSOC);

        return $user;
    }

    public function updateById($id, $user){
        $query = $this->connect->prepare("UPDATE $this->tableName SET 
                    first_name = '{$user['first_name']}', 
                    last_name = '{$user['last_name']}', 
                    email = '{$user['email']}', 
                    birthday = '{$user['birthday']}'
                     
                   WHERE id={$id}  
        ");

        $query->execute();
    }

    public function save($user){
        $sql = "INSERT INTO $this->tableName (first_name, last_name, email, password) VALUES ('{$user['first_name']}', '{$user['last_name']}', '{$user['email']}', '{$user['password']}')";
        $query = $this->connect->prepare($sql);
        $query->execute();
    }

    public function deleteById($id){
        $sql = "UPDATE {$this->tableName} SET delete_user = '1' WHERE id={$id}";
        $query = $this->connect->prepare($sql);
        $query->execute();
    }
}