<?php

use \Framework\Controller;

class UserController extends Controller
{

    public function usersAction()
    {
        $model = new User();
        $users = $model->getAll();
        $_POST['start_date'];
        $_POST['end_date'];
        $this->view->render('layout.php', 'user/users.php', ['users' => $users]);
    }

    public function editAction($id)
    {
        $model = new User();
        $user = $model->getById($id);

        $this->view->render('layout.php', 'user/user_page.php', ['user' => $user]);
    }

    public function addAction()
    {
        $this->view->render('layout.php', 'user/add_user.php');
    }

    public function saveAction()
    {
        $user = $_POST['user'];

        $model = new User();
        $model->save($user);

        header("Location:/user/users");
    }

    public function updateAction($id)
    {
        $model = new User();
        $user = $_POST['user'];
        $model->updateById($id, $user);

        header("Location:/user/users");
    }

    public function deleteAction($id)
    {
        $model = new User();
        $model->deleteById($id);

        header("Location:/user/users");
    }

    public function registerAction(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $userData = $_POST['user'];
            $model = new User();
            $user = $model->getByEmail($userData['email']);
            $errors = [];

            if(!isset($userData['first_name'])){
                $errors[] = 'First name обовязкове поле';
            }
            if(!isset($userData['last_name'])){
                $errors[] = 'Last name обовязкове поле';
            }
            if(!isset($userData['email'])){
                $errors[] = 'Email обовязкове поле';
            }
            if($user){
                $errors[] = 'Користувач з таким Email вже існує';
            }
            if($userData['password'] != $userData['confirm_password']){
                $errors[] = 'Паролі не співпадають';
            }
            if(strlen($userData['password']) < 6 or strlen($userData['password']) > 16){
                $errors[] = 'Довжина пароля має бути від 6 до 16 символів';
            }

            if(count($errors) > 0){
                $this->view->render('user/registration_form.php', null, ['errors' => $errors]);
            }else{
                $userData['password'] = md5(trim($userData['password']));
                $model->save($userData);

                $this->redirect('User', 'users');
            }

        }else{
            $this->view->render('user/registration_form.php', null);
        }
    }

    public function loginAction(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $userData = $_POST['user'];
            $model = new User();
            $user = $model->getByEmail($userData['email']);
            $errors = [];

            if(!$user){
                $errors[] = 'Користувачa з таким Email не існує';
            } elseif(md5(trim($userData['password'])) != $user['password']){
                $errors[] = 'Невірний пароль';
            }

            if(count($errors) > 0){
                $this->view->render('user/login_form.php', null, ['errors' => $errors]);
            }else{
                $_SESSION['user'] = [
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'email' => $user['email'],
                ];

                $this->redirect('User', 'users');
            }

        }else{
            $this->view->render('user/login_form.php', null);
        }
    }

    public function logoutAction(){
        unset($_SESSION['user']);
        $this->redirect('User', 'login');
    }

}