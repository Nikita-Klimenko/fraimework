<?php

use Framework\Controller;

class PostController extends Controller
{

    public function postsAction()
    {
        $model = new Post();
        $posts = $model->getAll();

        $this->view->render(
            'layout.php',
            'post/posts.php',
            [
                'posts' => $posts,
            ]
        );
    }

    public function getAction($postId)
    {
        $model = new Post();
        $commentModel = new Comment();
        $post = $model->getById($postId);
        $comments = $commentModel->getCommentsByPostId($postId);

        $this->view->render(
            'layout.php',
            'post/post.php',
            [
                'post' => $post,
                'comments' => $comments
            ]
        );
    }

    public function addAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            $this->view->render(
                'layout.php',
                'post/add_post.php',
                []
            );
        } else {
            $postModel = new Post();
            $postId = $postModel->save($_POST);

            ImageHelper::upload($_FILES['postPicture'], $postId);


            die();

            $this->redirect('PostController', 'getAction', [$postId]);
        }

    }

}