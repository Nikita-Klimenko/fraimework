<?php

class Validator {
    private $currentField = null,
        $errors = [],
        $fieldNames = [],
        $errMsg = [],
        $pass = null,
        $rules = [],
        $arrayForValidation;

    //database connecttion
    public function __construct() {
        $this->db = new QueryBuilder;
    }

    public function add($field, $fieldName = '', $rules = null) {
        //set field name
        $this->addField($field, $fieldName);
        if ($rules) {
            $this->addRules($rules);
        }
        return $this;
    }

    public function addField($field, $fieldName = '') {
        $this->currentField = $field;
        if (empty($fieldName)) {
            $this->fieldNames[$this->currentField] = $this->currentField;
        } else {
            $this->fieldNames[$this->currentField] = $fieldName;
        }
        return $this;
    }

    public function addRule($rule, $value = true) {
        $this->rules[$this->currentField][$rule] = $value;
        return $this;
    }

    public function addRules($rules) {
        //set rules
        foreach (explode('|', $rules) as $rules) {
            $rulesValues = explode(':', $rules);
            if (!isset($rulesValues[1])) {
                $rulesValues[1] = true;
            }
            $this->addRule($rulesValues[0], $rulesValues[1]);
            //add feature to implement errMsg
        }
        return $this;
    }

    public function addErrorMsg($rule, $message) {
        // 'match', 'Пароли не совпадают'
        $this->errMsg[$this->currentField][$rule] = $message;
        return $this;
    }

    public function check($arrayForValidation) {
        $this->arrayForValidation = $arrayForValidation;
        foreach ($this->rules as $field => $rules) {
            $this->currentField = $field;
            foreach ($rules as $this->rule => $value) {
                if (method_exists($this, $this->rule)) {
                    //calling rule functions
                    if (isset($arrayForValidation[$field])) {
                        call_user_func_array([$this, $this->rule], [$arrayForValidation[$field], $value]);
                    } else {
                        throw new Error('Input array doesn\'t contain the ' . strtoupper($item) . ' key!');
                    }
                } else {
                    throw new Exception('Validator doesn\'t have the ' . strtoupper($rule) . ' rule!');
                }
            }
        }
        if (empty($this->errors)) {
            return true;
        }
        return false;
    }

    public function getAllErrors() {
        return $this->errors;
    }

    public function getErrors() {
        foreach ($this->errors as $field => $error) {
            $this->errors[$field] = array_values($this->errors[$field])[0];
        }
        return $this->errors;
    }

    /**
     * Private functions below
     */
    private function addError($error) {
        // $field = 'Error';
        if (isset($this->errMsg[$this->currentField][$this->rule])) {
            return $this->errors[$this->currentField][$this->rule] = $this->errMsg[$this->currentField][$this->rule];
        }
        return $this->errors[$this->currentField][$this->rule] = $error;
    }

    /**
     * Rules function, returns true if item is passed and false in other case
     * Input array is available as $this->arrayForValidation.
     */


    private function require($item) {
        $item = Input::escape($item);
        if (empty($item)) {
            return $this->addError('Поле не должно быть пустым');
        }
    }

    private function max($item, int $value) {
        if (strlen($item) > $value) {
            return $this->addError("Поле должно быть меньше $value символов");
        }
    }

    private function min($item, $value) {
        if (strlen($item) < $value) {
            return $this->addError("Поле должно быть больше $value символов");
        }
    }

    private function match($item, $secItem) {
        if ($item != $this->arrayForValidation[$secItem]) {
            return $this->addError("Поля не совпадают");
        }
    }

    private function filter_username($item) {
        if (!preg_match("/^[a-zA-Z0-9]*$/",$item)) {
            return $this->addError("Неправильное имя пользователя");
        }
    }
}