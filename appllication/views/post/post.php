<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <h1><?=$post['title'];?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span>Published: <?=$post['created_at'];?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span>Author: <a href="/user/edit/<?=$post['author'];?>"><?=$post['first_name'] . ' ' . $post['last_name'];?></a>  </span>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <img src="<?=$post['path'];?>" id="post-id">
                <p><?=$post['text'];?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span>Views: <?=$post['views'];?> </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Comments:</h3>
<?php if ($comments): ?>
    <?php foreach ($comments as $comment):?>
        <div class="row">
            <div class="col-md-6">
                <hr>
                <span><a href="/user/edit/<?=$comment['author_id'];?>"><?=$post['first_name'] . ' ' . $post['last_name'];?></a></span>
                <br><span>Publish date: <?=$comment['created_at'];?></span>
                <p><?=$comment['text'];?></p>
            </div>
        </div>
    <?php endforeach;?>
<?php else:?>
    <p>No comments yet :(</p>
<?php endif;?>
<hr>
<div class="row">
    <div class="col-md-5">
        <form action="/comment/add" method="post" class="form-group">
            <input type="text" hidden value="<?=$post['id'];?>" name="postId">
            <label>Author</label>
            <select class="form-control" name="commentAuthorId">
                <option value="1">Biel Geyts</option>
                <option value="2">Nikola Tesla</option>
                <option value="3">Albert Einstein</option>
                <option value="4">Michael Faraday</option>
                <option value="5">Isaac Newton</option>
                <option value="6">Ernest Rutherford</option>
            </select>
            <label>Comment text</label>
            <textarea placeholder="Enter comment text" class="form-control" rows="8" name="commentText"></textarea>
            <br>
            <button type="submit" class="form-control btn btn-default" name="submit">Submit</button>
        </form>
    </div>
</div>
<hr>