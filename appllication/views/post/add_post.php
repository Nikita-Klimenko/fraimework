<div class="col-md-6 col-md-offset-3">
    <form action="/post/add" method="post" class="form-group" enctype="multipart/form-data">
        <label>Author: </label>
        <select class="form-control" name="postAuthorId">
            <option value="1">Biel Geyts</option>
            <option value="2">Nikola Tesla</option>
            <option value="3">Albert Einstein</option>
            <option value="4">Michael Faraday</option>
            <option value="5">Isaac Newton</option>
            <option value="6">Ernest Rutherford</option>
        </select>
        <label>Title: </label>
        <input type="text" name="postTitle" class="form-control">
        <label>Text: </label>
        <textarea placeholder="Post text" name="postText" class="form-control" rows="12"></textarea>
        <label>Image</label>
        <input type="file" name="postPicture">
        <br>
        <button type="submit" name="submit" class="form-control">Submit</button>
    </form>
</div>