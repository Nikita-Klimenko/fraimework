<?php

namespace Framework;


class OAuth extends Controller
{

    public static function checkUserSession()
    {
        if (!isset($_SESSION['user'])) {
            self::redirect('User', 'login');
        }
    }

}

?>