<?php

namespace Framework;

class Controller
{
    public $view;

    public function __construct()
    {
        $this->view = new View();
    }


    public function redirect($controllerName, $actionName, $params = [])
    {
        $controller = strtolower(str_replace('Controller', '', $controllerName));
        $action = str_replace('Action', '', $actionName);
        $parameters = implode('/', $params);
        if ($params) {
            header("location: /{$controller}/{$action}/{$parameters}");
        } else {
            header("location: /{$controller}/{$action}");
        }
    }
}

?>